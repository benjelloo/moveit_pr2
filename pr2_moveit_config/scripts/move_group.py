#!/usr/bin/env python
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

def initialize():
    #initialize moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)
    #instantiate a rospy node
    rospy.init_node('move_group_python_interface',anonymous=True)

    robot=moveit_commander.RobotCommander()

    scene = moveit_commander.PlanningSceneInterface()

    # Check RViz for other group names, if desired (roslaunch pr2_moveit_config demo.launch)
    group_name = "right_arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # DisplayTrajectory publisher is used to display trajectories in RViz
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                    moveit_msgs.msg.DisplayTrajectory,
                                                    queue_size=20)

    # We can get the name of the reference frame for this robot:
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # We can also print the name of the end-effector link for this group:
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # We can get a list of all the groups in the robot:
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Sometimes for debugging it is useful to print the entire state of the robot:
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")

    return move_group,robot,display_trajectory_publisher


def pose_goal():
    #Planning to a pose goal
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = 1.0
    pose_goal.position.x = 0.4
    pose_goal.position.y = 0.1
    pose_goal.position.z = 0.4

    move_group.set_pose_target(pose_goal)

    #Execute the pose goal
    plan = move_group.go(wait=True)
    # Calling `stop()` ensures that there is no residual movement
    move_group.stop()
    # It is always good to clear your targets after planning with poses.
    # Note: there is no equivalent function for clear_joint_value_targets()
    move_group.clear_pose_targets()

#CARTESIAN PATHS
def plan_cartesian_path(scale=1):
    waypoints = []

    wpose = move_group.get_current_pose().pose
    print(wpose)
    wpose.position.z -= scale * 0.1  # First move up (z)
    wpose.position.y += scale * 0.2  # and sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y -= scale * 0.1  # Third move sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    # We want the Cartesian path to be interpolated at a resolution of 1 cm
    # which is why we will specify 0.01 as the eef_step in Cartesian
    # translation.  We will disable the jump threshold by setting it to 0.0,
    # ignoring the check for infeasible jumps in joint space, which is sufficient
    # for this tutorial.
    (plan, fraction) = move_group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.01,        # eef_step
                                    0.0)         # jump_threshold

    # Note: We are just planning, not asking move_group to actually move the robot yet:
    return plan, fraction

def display_trajectory(plan):
    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(plan)
    # Publish
    display_trajectory_publisher.publish(display_trajectory)

def execute_plan(plan):
    move_group.execute(plan,wait=True)

  
move_group,robot,display_trajectory_publisher=initialize()

move_group_active_joints = move_group.get_active_joints()
move_group_variable_values = move_group.get_current_joint_values()

print("Active Joints: ",move_group_active_joints)
print ("============ Initial Joint values: ", move_group_variable_values)

print("=====Robot will move to a specified pose goal=====")
input("Press Enter to continue: ")
#pose_goal()
print("=====Plan cartesian path=====")
input("Press Enter to continue: ")
cartesian_plan,fraction = plan_cartesian_path()
print("=====Display Trajectory=====")
input("Press Enter to continue: ")
display_trajectory(cartesian_plan)
print("=====Execute Plan=====")
input("Press Enter to continue: ")
execute_plan(cartesian_plan)

move_group_variable_values = move_group.get_current_joint_values()
print ("============ Final Joint values: ", move_group_variable_values)
print("============ Final Pose: ",move_group.get_current_pose().pose)
print("Code has finished executing successfully!")


