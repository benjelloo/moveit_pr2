#!/usr/bin/env python
from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math

# Run this script: python joint_sweeper.py joint_name
    # Use python2 if python3 is the system default
# If using rosrun pr2_moveit_config joint_sweeper.py to run, set the joint_name manually

def initialize():
    #initialize moveit_commander
    moveit_commander.roscpp_initialize(sys.argv)
    #instantiate a rospy node
    rospy.init_node('move_group_python_interface',anonymous=True)

    robot=moveit_commander.RobotCommander()

    scene = moveit_commander.PlanningSceneInterface()

    # Check RViz for other group names if desired (roslaunch pr2_moveit_config demo.launch)
    group_name = "right_arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # DisplayTrajectory publisher is used to display trajectories in RViz
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                    moveit_msgs.msg.DisplayTrajectory,
                                                    queue_size=20)

    # We can get the name of the reference frame for this robot:
    planning_frame = move_group.get_planning_frame()
    print("============ Planning frame: %s" % planning_frame)

    # We can also print the name of the end-effector link for this group:
    eef_link = move_group.get_end_effector_link()
    print("============ End effector link: %s" % eef_link)

    # We can get a list of all the groups in the robot:
    group_names = robot.get_group_names()
    print("============ Available Planning Groups:", robot.get_group_names())

    # Sometimes for debugging it is useful to print the entire state of the robot:
    print("============ Printing robot state")
    print(robot.get_current_state())
    print("")

    return move_group,robot,display_trajectory_publisher

def sweep(group_variable_values,joint):
    # Zero all joints
    for x in range(len(group_variable_values)):
        group_variable_values[x]=0.0

    # These joints aren't valid at 0.0    
    group_variable_values[3]=-0.15
    group_variable_values[5]=-0.1

    if joint=="r_shoulder_pan_joint":
        angle_array = [-math.pi/2,-math.pi/4,-math.pi/8,0.0]
        ind = 0
    elif joint=="r_shoulder_lift_joint":
        angle_array = [0.0,math.pi/8,math.pi/6,math.pi/4]
        ind = 1
    elif joint=="r_upper_arm_roll_joint":
        angle_array = [-math.pi,-math.pi/2,-math.pi/4,0.0]
        ind = 2
    elif joint=="r_elbow_flex_joint":
        angle_array = [-0.15,-math.pi/4,-math.pi/2,-math.pi*2/3]
        ind = 3
    elif joint=="r_forearm_roll_joint":
        angle_array=[-math.pi,-math.pi/2,0,math.pi/2]
        ind=4
    elif joint=="r_wrist_flex_joint":
        angle_array=[-0.1,-math.pi/4,-math.pi/3,-math.pi/2]
        ind=5
    elif joint=="r_wrist_roll_joint":
        angle_array=[-math.pi,-math.pi/2,0,math.pi/2]
        ind=6

    for angle in angle_array:
        print('\n ===== ANGLE:',angle,' ===== \n')
        group_variable_values[ind]=angle
        move_group.set_joint_value_target(group_variable_values)
        plan=move_group.go(wait=True)
        move_group.stop()

        print("Final Joint Values: ", move_group.get_current_joint_values())
        print("Final Endeff Location: \n",move_group.get_current_pose().pose)

        for x in range(30,37):
            print(robot.get_current_state().joint_state.name[x],':',robot.get_current_state().joint_state.position[x])

move_group,robot,display_trajectory_publisher=initialize()

move_group_active_joints = move_group.get_active_joints()
move_group_variable_values = move_group.get_current_joint_values()

print("Active Joints: ",move_group_active_joints)
print ("Initial Joint values: ", move_group_variable_values)

sweep(move_group_variable_values,"r_shoulder_pan_joint")

#use the following if running the script through `python joint_sweeper.py JOINTNAME`, otherwise set manually above
#sweep(move_group_variable_values, sys.argv[1])

'''
print("Final Joint States: ")
for x in range(len(robot.get_current_state().joint_state.name)):
    print(robot.get_current_state().joint_state.name[x],':',robot.get_current_state().joint_state.position[x])
'''